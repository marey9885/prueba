import '../App.css';
import './Contador.css';

function Contador({ total, completed }) {
  return (
    <h2>
      Has completado {completed} de {total} tareas
    </h2>
  );
}

export { Contador };
