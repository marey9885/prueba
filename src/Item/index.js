/* eslint-disable no-undef */
import '../App.css';
import './Item.css';

import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import CheckIcon from '@mui/icons-material/Check';
import { Box } from '@mui/material';

function Item(props) {
  return (
    <li
      style={{
        display: 'flex',
        backgroundColor: '#ECE5E5',
        alignContent: 'center',
        justifyContent: 'center',
        marginBottom: '10px',
        borderRadius: '10px',
      }}
    >
      <Box>
        <IconButton
          completed={props.completar}
          onComplete={() => props.completarTarea(props.text)}
          onClick={props.onComplete}
        >
          <CheckIcon />
        </IconButton>
      </Box>
      <Box>
        <p>{props.text}</p>
      </Box>
      <Box>
        <IconButton
          edge='end'
          aria-label='delete'
          //completed={props.completar}
          borrar={() => props.eliminarTarea(props.text)}
          onClick={props.borrar}
        >
          <DeleteIcon />
        </IconButton>
      </Box>
    </li>
  );
}

export { Item };
